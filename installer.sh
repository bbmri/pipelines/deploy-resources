#!/usr/bin/env python
import os
import subprocess
import socket
import shutil
import argparse

# Merge the dirs using symbolic links - subsequent duplicates are ignored
def merge_dirs(list_dirs_to_merge, target_dir):
    for dir in list_dirs_to_merge:
        contents = os.listdir(dir)
        for target in contents:
            full_target = os.path.join(target_dir, target)
            full_source = os.path.join(dir, target)
            if not os.path.exists(full_target):
                os.symlink(full_source, full_target)

def cleanup(dir):
	shutil.rmtree(os.path.join(dir, 'ansible'), ignore_errors=True)
	shutil.rmtree(os.path.join(dir, 'deploy-resources-bbmri'), ignore_errors=True)
	shutil.rmtree(os.path.join(dir, 'deploy-resources'), ignore_errors=True)
	if os.path.exists(os.path.join(dir, 'inventory.txt')):
		os.remove(os.path.join(dir, 'inventory.txt'))
			
def get_deploy_resources():
        gitlab_user = os.getenv('USER', '?')
        gitlab_user = raw_input('Please enter your gitlab user id [{0}]: '.format(gitlab_user)) or gitlab_user   
        
        deploy_resources_git = 'https://{0}@gitlab.com/bbmri/pipelines/deploy-resources.git'.format(gitlab_user)
        print ('Fetching deploy resources from: {0} with user {1} ........'.format(deploy_resources_git, gitlab_user))
        output = subprocess.call(['git', 'clone', deploy_resources_git])
        if not output == 0 or not os.path.exists('./deploy-resources'):
            print ('Could not retrieve the deploy-resources, exiting...')
            exit(1)
            
        role_dirs_to_merge = [os.path.abspath('./deploy-resources/ansible/roles')]
        ansible_dir_to_merge = [os.path.abspath('./deploy-resources/ansible')]
        deploy_resources_bbmri_git = 'https://{0}@gitlab.com/bbmri/pipelines/deploy-resources-bbmri.git'.format(gitlab_user)
        print ('Attempting to fetch private deploy resources from: {0} with user {1} ........'.format(deploy_resources_bbmri_git, gitlab_user))
        output = subprocess.call(['git', 'clone', deploy_resources_bbmri_git])
        print "output: ", output
        if not os.path.exists('./deploy-resources-bbmri'):
            print ('Could not retrieve the deploy-resources, the BBMRI private resources will not be available...')         
        else:
            role_dirs_to_merge.append(os.path.abspath('./deploy-resources-bbmri/ansible/roles'))
            ansible_dir_to_merge.append(os.path.abspath('./deploy-resources-bbmri/ansible'))
        # merge both public and private ansible content into one for ease of install  (ansible can't handle multiple role dirs)          
        os.makedirs('./ansible/roles')
        merge_dirs(role_dirs_to_merge, os.path.abspath('./ansible/roles'))
        merge_dirs(ansible_dir_to_merge, os.path.abspath('./ansible'))
    
def install():
    try:
        # Should ass option to simply refresh the repositories (auto-magically)
        parser = argparse.ArgumentParser(description='Install BBMRI')
        parser.add_argument('--clean', action='store_true', help='clean old install attempt before starting')	
        args = parser.parse_args()
        if args.clean:
            cleanup(os.path.abspath(os.curdir))
        print('Welcome to the BBMRI installer!')
        print('Please give your install preferences or <Enter> to accept the defaults...\n')
        target_system = 'localhost'
        target_system = raw_input('On which system do you wish to install BBMRI? [{0}]: '.format(target_system)) or target_system
        print ('Installing on system: {0}\n'.format(target_system))
        install_user = os.getenv('USER', '?')
        install_user = raw_input('Under which username will you do the install? [{0}]: '.format(install_user)) or install_user
        print ('Install under user: {0}\n'.format(install_user))
        install_directory = os.path.join('/home', install_user, '.local/bbmri')
        install_directory = raw_input('Where will the install be performed? [{0}]: '.format(install_directory)) or install_directory
        print ('Install in directory: {0}\n'.format(install_directory))
        
        if not os.path.exists('./deploy-resources'):
            get_deploy_resources()
            print('Deployment resources retrieved.')
        else:
            print ('Deployment resources already present')
        
        target_ip = socket.gethostbyname(target_system)
        print('Starting the BBMRI install')
        user_option = []
        connection_option = []
        if target_system == 'localhost':
            print('Install target: localhost')
            connection_option = ['--connection=local']
            with open('./inventory.txt', 'w') as f:
                f.seek(0, 0)
                f.truncate()
                f.write('localhost              ansible_connection=local')
                
        else:
            user_option = ['-u', install_user, '-k']
            print('Remote install target: {0} with user: {1}'.format(target_system, install_user))
            with open('./inventory.txt', 'w') as f:
                f.seek(0, 0)
                f.truncate()
                f.write('target ansible_host={0} ansible_port=22'.format(target_ip))

        subprocess.call(['ansible-playbook'] + user_option + ['-v', '-e', '"root_prefix={0}"'.format(install_directory),
            '--ssh-extra-args=-o PasswordAuthentication=true -o PreferredAuthentications=password -o KbdInteractiveAuthentication=yes -o StrictHostKeyChecking=no', 
            '-i', '../inventory.txt', 'install-requirements.yml'] + connection_option, cwd='./ansible')

                
    except BaseException as e:
        print ('Install threw error {0}'.format(str(e)))

if __name__ == '__main__':
    install()

