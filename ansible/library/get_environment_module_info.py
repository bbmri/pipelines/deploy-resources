#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: get_environment_module_info

short_description: Return information about the environment module system if present

version_added: "2.4"

description:
    - "If the environment modules system is installed this will retrieve the 
    following information:
        module tool  - either Lmod, EnvironmentModulesC or EnvironmentModulesTcl"
        module syntax - either Lua or Tcl
        tool path - If the tool is $LMOD_CMD this is an empth string otherwise it contains the path to the module tool

options:
    None

notes:
    Inspired by ansible ipfy_facts.py

author:
    - B. van Lew (@ybldrvnlew)
'''

EXAMPLES = '''
# Gather environment module info from the install host
- name Get the environment module info
  get_environment_module_info
'''

from ansible.module_utils.basic import *
import re
import os

class ModuleInfo(object):
    
    def __init__(self):
        # Can retrieve any params from the global module here
        pass
    
    def run(self):
        """ Return a 3-tuple containing (modpath, modsyntax, toolpath)
            modpath is string indentifying the module system installed
           The possible  modpath result strings are:
            Lmod
            EnvironmentModulesC
            EnvironmentModulesTcl
          The possible results for modsyntax are Tcl, Lua
          For toolpath contains wither the path for the module or module.tcl command or None if the command is $LMOD_CMD
            Returns None if the module type can't be detected
            This function is meant to replace the check_module_command
            in https://raw.githubusercontent.com/hpcugent/easybuild-framework/develop/easybuild/scripts/bootstrap_eb.py
            With a simple but more widely applicable solution
        """
        result = {
            'environment_module_info_tool': None,
            'environment_module_info_syntax': None,
            'environment_module_info_path': None,                        
        }    
        from subprocess import Popen, PIPE, STDOUT
        shell_command = 'bash -c "type module"'
        p = Popen(shell_command, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        modtool, modsyntax, toolpath = None, None, None
        if None is re.search(r'(\.tcl)', output):
            if None is re.search(r'(modulecmd)', output):
                if not None is re.search(r'.*(\$LMOD_CMD).*', output):
                    result['environment_module_info_tool'] = "Lmod"
                    result['environment_module_info_syntax'] = "Lua"
                    result['environment_module_info_path'] = ""
            else:
                match = re.search(r'eval `(\S*)/modulecmd .*`', output)
                result['environment_module_info_path'] = os.path.expandvars(match.groups()[0])
                result['environment_module_info_tool'] = "EnvironmentModulesC"
                result['environment_module_info_syntax'] = "Tcl"
        else:
            result['environment_module_info_tool'] = "EnvironmentModulesTcl"
            match = re.search(r'eval `(\S*)/modulecmd\.tcl .*`', output)
            result['environment_module_info_path'] = os.path.expandvars(match.groups()[0])
            result['environment_module_info_syntax'] = "Tcl"
            
        return result
        
def main():

    # Empty argument_spec - simple interface for now
    global module
    module = AnsibleModule(
        argument_spec = dict(
        ),
        supports_check_mode=True,
    )
    
    module_info = ModuleInfo().run()
    module_info_result = dict(changed=False, ansible_facts=module_info)
    module.exit_json(**module_info_result)

if __name__ == "__main__":
    main()
