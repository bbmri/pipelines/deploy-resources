#!/bin/bash
PARAMS="$@"

echo **Params to playbook:
echo "$PARAMS"

# If needed do clever stuff with the params here
#e.g. if you are using absolute paths regex your user path to UNIX style path otherwise ansible fails
#PARAMS_LINUX=${PARAMS/<winvol>:\/<cygwinroot>\/home\/<USER>/\~}

# A handy place to set ANSIBLE specific environment variables
export ANSIBLE_SSH_ARGS='-o ControlMaster=no -o UserKnownHostsFile=/dev/null'
export ANSIBLE_SCP_IF_SSH=False
export ANSIBLE_NOCOLOR=1 
# together with UserKnowHostsFile = /dev/null stops DNS spoofing errors 
# when test vms are create destroyed and recreated
# export ANSIBLE_HOST_KEY_CHECKING=False
/usr/bin/ansible-playbook $PARAMS