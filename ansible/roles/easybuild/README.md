Role Name
=========

This role install lua/lmod and easybuild without using root privileges.

lua and lmod are build from source and easybuild is installed using pip

Requirements
------------

The remote server must have python (2.7) and allow the creation of a virtualenv. 

To permit the build of lua and lmod the following are required: 

1. On Debian type (including Ubuntu) systems the build-essential package must be installed
2. On Redhat type (including CentOS, Scientific Linux) systems the  'Development Tools' must be installed

Role Variables
--------------

A description of the settable variables for this role should go here, including 
any variables that are in defaults/main.yml, vars/main.yml, and any variables 
that can/should be set via parameters to the role. Any variables that are read 
from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

The path for virtualenv should be settable.
The user to use for the install is settable

Dependencies
------------

No other role dependencies

Example Playbook
----------------

Including an example of how to use your role (for instance, with 
variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: easybuild
          install_user: "{{install_username}}"
          virtualenv: "{{virtualenv_path}}"

License
-------

BSD

Author Information
------------------

B. van Lew b.van_lew@lumc.nl
