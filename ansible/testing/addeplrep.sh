# find out the release (version) of your RHEL e.g. 6.5
# this file is only for version 6 - the version could be
# passed as a script parameter 
# Idea from http://cutler.io/2012/07/installing-centos-packages-on-red-hat-enterprise-linux/

# adapted from http://johnpoelstra.com/rhel-6-epel/
# adding the epl repo to allow install of python-pip

cat > /etc/yum.repos.d/epl.repo << EOL
[epel]
name=Extra Packages for Enterprise Linux 6 - $basearch
baseurl=http://ftp.nluug.nl/pub/os/Linux/distr/fedora-epel/6/\$basearch/
#mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=epel-6&arch=\$basearch
enabled=1
gpgcheck=0
EOL

# save centos.repo and (optional) run this to reset the cache

yum clean all