# find out the release (version) of your RHEL e.g. 6.5
# this file is only for version 6 - the version could be
# passed as a script parameter 
# Idea from http://cutler.io/2012/07/installing-centos-packages-on-red-hat-enterprise-linux/

# create a new yum .repo config file
# add the following contents to centos.repo
# change the 6 in baseurl to your RHEL release
# check here for options: http://ftp.heanet.ie/pub/centos/

cat > /etc/yum.repos.d/centos.repo << EOL
[centos]
name=CentOS \$releasever - \$basearch
baseurl=http://ftp.heanet.ie/pub/centos/6/os/\$basearch/
enabled=1
gpgcheck=0
EOL
# save centos.repo and (optional) run this to reset the cache

yum clean all