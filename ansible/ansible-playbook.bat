@echo off

REM If you used the stand Cygwin installer this will be C:cygwin
set CYGWIN=F:\cygwin64\bin

REM You can switch this to work with bash with %CYGWIN%binbash.exe
set SH=%CYGWIN%\bash.exe

REM running bash as login sets the environment correctly to allow ansible to running
REM cd to the directory containing the playbook to get the relative paths correctly
REM note that cygwin can deal with paths containing  a windows volume such as D:/
"%SH%" --login -c "cd '%CD%' && ls && ./ansible-winpath-playbook.sh %*"