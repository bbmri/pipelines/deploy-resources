#!/usr/bin/env sh
if [ "$#" -ne 1 ] || ! [ -d "$1" ]; then
  echo "Usage: $0 VIRTUALENV_DIRECTORY" >&2
  echo "Supply a shared directory for the installation" >&2  
  exit 1
fi
pwd=`pwd`

# setup the python virtualenv for ansible/easybuild/fastr
mkdir $1
cd $1
pip install virtualenv
mkdir fastr_env
cd fastr_env
virtualenv fastr_env
# switch to the fastr_env virtualenv
source $1/fastr_env/bin/activate
pip install ansible

# return to the start directory and run ansible
cd $pwd
ansible-playbook -i inventory.txt -e fastr_prefix=$1 ./tasks/fastr-playbook.yml

