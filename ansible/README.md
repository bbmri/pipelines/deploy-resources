# Ansible roles for easybuild, fastr bootstrap and fastr tools
=============================================================

## Use case: Non-sudo user installing & running fastr
-----------------------------------------------------
In it's initial version this provisioning system targets a single user case: and end user without sudo rights want to install and use fastr on their cluster. 

The system may be expanded to support the cloud installation case or shared installation cases.  

## Design of the provisioning system
------------------------------------
Each pipeline will have an ansible role. This role will install all the pipeline tools using easybuild-easyconfig files. These are currently hosted on https://git.lumc.nl/bvanlew/fastr-tools-easyconfigshttps://git.lumc.nl/bvanlew/fastr-tools-easyconfigs.

## Requirements
------------

## Systems supported: 
--------------------
The roles presented are supported on the following systems: 

- CentOS >= 7.2 or Redhat >= 6.5 or Ubuntu >= 14.04 or Scientific Linux >= 6.5

The provisioning host must have ansible installed (see note on Windows/Linux differences below). The targets should have the software listed under Prerequisites (see below)

### Prerequisites: 
The following prerequisites are required for easybuild and fastr/fastr-tools. 

1. build-essential/@development
2. tcl  
3. python-dev/python-devel 
4. pip
5. virtualenv

During testing they are installed by the testing-prerequisties.yml playbook called by the Vagrantfile.

## Testing hosts with Vagrant: 
------------------------------
The repository includes a Vagrantfile that can be used to setup a set 
of test systems to debug the provisioning.

Some Vagrant images may need to have their guest additions upgraded for VirtualBox. To that end the vagrant-vbguest plugin should be installed.

~~~
vagrant plugin install vagrant-vbguest
~~~

See https://www.vagrantup.com/docs/plugins/usage.html for more info.

## Running the bootstrap test
----------------------------
### Create test systems 
----------------------
For testing purposes run the Vagrantfile. This creates several initially empty
hosts and creates a test user and installs the prerequisites in a provisioning step.

~~~
vagrant up --provision
~~~

If you waant to test a subset then you can modify the Vagrantfile

### Testing provisioning
------------------------
Next a second provisioning step (see runtest.bat) tests the easybuild and fastr roles. If this succeeds 
the test user (swithing to the test user is not yet enable) will have easybuild, fastr and
fastr-tools installed in a directory that they specify.

#### Windows/Linux note
----------------------
Initial development work was done using a Windows host running Ansible via cygwin. This accounts for the runtest.bat. On a Linux provisioning host the ansible-playbook command can be called directly.

## Using the roles
---------------

To actually use the roles in a real environment you can follow the example 
in test-playbook.yml (overriding role parameters as needed). The contents look like this:

~~~
---
- name: Run the easybuild, fastr and GAMEs setup 
  hosts: all
  remote_user: bbmri-wp3
  become: no
  roles:
  - role: easybuild
  - role: fastr
  - role: GAMEs
~~~

Note that the roles easybuild and fastr are required roles.
 
### Role parameters
---------

In the roles/easybuild/defaults/main.yml and roles/fastr/defaults/main.yml
there a variable that determines the root of the whole system

  root_prefix: {{ ansible_env.HOME }}/.local/fastr
  
Where ansible_env.HOME is the $HOME of the logged in user

In the roles/easybuild a variable determines if the python virtual environment and lmod are configured and available at login time. This is achieved by adding the appropriate PATH modiications and source active to the .bashrc of the user.

  set_modules_for_easybuild: True
  
When invoking the roles these "role defaults" may be changed if desired, see the examples of role parameters at http://docs.ansible.com/ansible/playbooks_roles.html#role-default-variables

### Ansible quick start

To install ansible on a provisioning host first install ansible (i not already available) :

~~~
pip install ansible   # (to private virtual environment if needed)
~~~

To run a playbook: 

~~~
ansible-playbook -u <USERNAME> -k -i inventory.txt playbook.yml
~~~

Where inventory is the list of hosts to provision (for a private fastr install this might be a single node where the <USERNAME> user has an account ). Inventory.txt give the details needed to loog in (see test-inventory.txt for reference) Playbook.tml is the list of roles including easybuild, fastr and the all the pipeline roles you wish to install. You will be prompted for the password for <USERNAME>

See https://wiredcraft.com/blog/getting-started-with-ansible-in-5-minutes/ for an slightly longer introduction or the ansible docs https://www.ansible.com/get-started









## Developer notes and issues

### pip install and/or EasyBuild

For some base packages, (e.g. fastr) simply doing pip install is a 
good option. This is done from Ansible but could be called from an easyconfig file. 
The standard easyconfigs repository in github hpcugent/easybuild-easyconfigs contains
examples (search for 'use_pip = True') 


### Incorrect source downloads with easybuild

How to work around EasyBuild packages where the source tarball downloads are
incorrect (e.g., `GCCcore/gmp-6.0.0a.tar.bz2` for which an HTML page is
downloaded)?

Possible solutions:

1. Patch the recipe.
2. Download the tarball separately.
3. Include the tarball with the playbook.

This issue was noted in the original repository we cloned this from and
remains relevant for us - tests show that builds that try to pull the base
tools often pull a website instead.

### Fastr tool installation 

There are a number of options for installing fastr tools using EasyBuild packages. 
Options:

1. Just use tasks like the following:
   `shell: . {{ easybuild_root }}/modules/modules.bashrc && module load EasyBuild && eb SOME_PACKAGE --robot`
2. Have a custom `easybuild` Ansible module which wraps this.
3. Have a `easybuild_packages` variable with a list of packages to be
   installed by this role.
4. Similar, but as a separate `easybuild-packages` role which is cheaper to
   repeatedly include from other roles.

Start simple with option 1 and later choose between 2 and 4.
Preferably the final solution should provide idempotency.

A separate repository for fastr tools easybuild-easyconfigs has 
already been created. See repository: fastr-tools-easyconfig

Each fastr tool should create an ansible role that includes the 
fastr role as a dependency (the fastr role has easybuild rolw as a dependency).
So the end user can simply mix and match tools roles known that all prerequisites
will be installled.

### Custom EasyBuild configs

How best to integrate custom EasyBuild configs? As separate role per set of
configs? Are these configs stored alongside the 'normal' configs? This issues
is relevant for non-public packages such as GAMEs
