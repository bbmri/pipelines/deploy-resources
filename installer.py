#!/usr/bin/env python
import os
import sys
import subprocess
import socket
import shutil
import argparse
import json
import getpass
import ansible
from collections import namedtuple
from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback import CallbackBase

# Encrypt user passwords for the vault as follows 
# ansible-vault encrypt_string 'passwordstring' --name 'user_pass' --output=-

class SaveEnvironFactsAsVariablesCallback(CallbackBase):
    """The environment facts are saved as a variables"""

    def __init__(self, filename):
        super(SaveEnvironFactsAsVariablesCallback, self).__init__()
        # store all results
        self.state = None
        self.results = []
        self.filename = filename

    def v2_runner_on_ok(self, result, **kwargs):
        """Save result instead of printing it"""
        self.results.append(result)
        facts = result._result['ansible_facts']
        with open(self.filename, 'w') as fp:
            json.dump({'modules_tool': facts['environment_module_info_tool'],
                       'module_path': facts['environment_module_info_path'],
                       'module_syntax': facts['environment_module_info_syntax']
                       }, fp)
            print('Environment module information saved in: {0}'.format(self.filename))

    def v2_runner_on_failed(self, result, ignore_errors=False):
        self.state = 'failed'
        self.results.append(result)
        with open(self.filename, 'w') as fp:
            json.dump({'modules_tool': '<EnvironmentModulesC or EnvironmentModulesTcl or Lmod>',
                       'module_path': '<path to the module tool binary>',
                       'module_syntax': '<Tcl or Lua>'
                       }, fp)
        print('Failed to get environment module information, edit {0} manually'.format(self.filename))



def save_environment_info(host, user, password, filename='./environment_module.json', module_path='./'):
    module_path = os.path.join(ansible.__path__[0], 'modules') + ':' + os.path.abspath(module_path)
    connection = 'local' 
    passwords = dict()
    if host != 'localhost':
        connection = 'ssh' 
        passwords = dict(conn_pass=password)

    Options = namedtuple('Options',
        ['connection', 'module_path', 'forks', 
        'remote_user', 'private_key_file', 'become', 'become_method', 'become_user',
        'ssh_common_args','sftp_extra_args', 'scp_extra_args', 'ssh_extra_args',
        'verbosity', 'check']
    )
    options = Options(
        connection=connection,
        module_path=module_path,
        forks=1,
        remote_user=user,
        private_key_file=None,
        ssh_common_args=None,
        ssh_extra_args=None,
        sftp_extra_args=None,
        scp_extra_args=None,
        become=None,
        become_user=None,
        become_method=None,
        verbosity=3,
        check=False)

    loader = DataLoader()
    variable_manager = VariableManager()
    inventory = Inventory(loader=loader, variable_manager=variable_manager, host_list=[host])
    variable_manager.set_inventory(inventory)

    
    # dynamically define a play to return the environment module info facts
    play_source =  dict(
        name = "Ansible Play",
        hosts = [host],
        gather_facts = 'no',
        tasks = [ dict(action=dict(module='get_environment_module_info')) ]
    )
    play = Play().load(play_source, variable_manager=variable_manager, loader=loader)
    results_callback = SaveEnvironFactsAsVariablesCallback(os.path.abspath(filename))

    # execute the play to get the environment info, the callback will prompt the use if it fails
    tqm = TaskQueueManager(
        inventory=inventory,
        variable_manager=variable_manager,
        loader=loader,
        options=options,
        passwords=passwords,
        stdout_callback=results_callback,
    )
    try:
        ret = tqm.run(play)
        # print results_callback.results[0]._host, results_callback.results[0]._result['ansible_facts'],
    finally:
        tqm.cleanup()


# Merge the dirs using symbolic links - subsequent duplicates are ignored
def merge_dirs(list_dirs_to_merge, target_dir):
    for dir in list_dirs_to_merge:
        contents = os.listdir(dir)
        for target in contents:
            full_target = os.path.join(target_dir, target)
            full_source = os.path.join(dir, target)
            if not os.path.exists(full_target):
                os.symlink(full_source, full_target)

def cleanup(dir):
	shutil.rmtree(os.path.join(dir, 'ansible'), ignore_errors=True)
	shutil.rmtree(os.path.join(dir, 'deploy-resources-bbmri'), ignore_errors=True)
	shutil.rmtree(os.path.join(dir, 'deploy-resources'), ignore_errors=True)
	if os.path.exists(os.path.join(dir, 'inventory.txt')):
		os.remove(os.path.join(dir, 'inventory.txt'))
			
def get_deploy_resources():
        gitlab_user = os.getenv('USER', '?')
        gitlab_user = raw_input('Please enter your gitlab user id [{0}]: '.format(gitlab_user)) or gitlab_user   
        
        deploy_resources_git = 'https://{0}@gitlab.com/bbmri/pipelines/deploy-resources.git'.format(gitlab_user)
        print ('Fetching deploy resources from: {0} with user {1} ........'.format(deploy_resources_git, gitlab_user))
        output = subprocess.call(['git', 'clone', deploy_resources_git])
        if not output == 0 or not os.path.exists('./deploy-resources'):
            print ('Could not retrieve the deploy-resources, exiting...')
            exit(1)
            
        role_dirs_to_merge = [os.path.abspath('./deploy-resources/ansible/roles')]
        ansible_dir_to_merge = [os.path.abspath('./deploy-resources/ansible')]
        deploy_resources_bbmri_git = 'https://{0}@gitlab.com/bbmri/pipelines/deploy-resources-bbmri.git'.format(gitlab_user)
        print ('Attempting to fetch private deploy resources from: {0} with user {1} ........'.format(deploy_resources_bbmri_git, gitlab_user))
        output = subprocess.call(['git', 'clone', deploy_resources_bbmri_git])
        print "output: ", output
        if not os.path.exists('./deploy-resources-bbmri'):
            print ('Could not retrieve the deploy-resources, the BBMRI private resources will not be available...')         
        else:
            role_dirs_to_merge.append(os.path.abspath('./deploy-resources-bbmri/ansible/roles'))
            ansible_dir_to_merge.append(os.path.abspath('./deploy-resources-bbmri/ansible'))
        # merge both public and private ansible content into one for ease of install  (ansible can't handle multiple role dirs)          
        os.makedirs('./ansible/roles')
        merge_dirs(role_dirs_to_merge, os.path.abspath('./ansible/roles'))
        merge_dirs(ansible_dir_to_merge, os.path.abspath('./ansible'))

    
def install():
    try:
        # Should ass option to simply refresh the repositories (auto-magically)
        parser = argparse.ArgumentParser(description='Install BBMRI')
        parser.add_argument('--clean', action='store_true', help='clean old install attempt before starting')	
        args = parser.parse_args()
        if args.clean:
            cleanup(os.path.abspath(os.curdir))
        print('Welcome to the BBMRI installer!')
        print('Please give your install preferences or <Enter> to accept the defaults...\n')
        target_system = 'localhost'
        target_system = raw_input('On which system do you wish to install BBMRI? [{0}]: '.format(target_system)) or target_system
        print ('Installing on system: {0}\n'.format(target_system))
        install_user = os.getenv('USER', '?')
        install_user = raw_input('Under which username will you do the install? [{0}]: '.format(install_user)) or install_user
        print ('Install under user: {0}\n'.format(install_user))
        install_directory = os.path.join('/home', install_user, '.local/bbmri')
        install_directory = raw_input('Where will the install be performed? [{0}]: '.format(install_directory)) or install_directory
        print ('Install in directory: {0}\n'.format(install_directory))
        
        if not os.path.exists('./deploy-resources'):
            get_deploy_resources()
            print('Deployment resources retrieved.')
        else:
            print ('Deployment resources already present')
        
        target_ip = socket.gethostbyname(target_system)
        print('Starting the BBMRI install on IP: ', target_ip)
        user_option = []
        connection_option = []
        connection_password = ''
        if target_system == 'localhost':
            print('Install target: localhost')
            connection_option = ['--connection=local']
            with open('./inventory.txt', 'w') as f:
                f.seek(0, 0)
                f.truncate()
                f.write('localhost              ansible_connection=local')
                
        else:
            connection_password = getpass.getpass('Enter the login password for user {0} on {1}: '.format(install_user, target_system)) or connection_password
            passwordstr = json.dumps({"ansible_password": "{0}".format(connection_password)})
            user_option = ['-u', install_user, '--extra-vars', passwordstr]
            print('Remote install target: {0} with user: {1}'.format(target_system, install_user))
            # print ('useroption: ', user_option)
            with open('./inventory.txt', 'w') as f:
                f.seek(0, 0)
                f.truncate()
                f.write('target ansible_host={0} ansible_port=22'.format(target_ip))
        # get the environment info
        save_environment_info(target_system, install_user, connection_password, filename='./environment_module.json', module_path='./ansible/library')
        # pass the extra args to the ansible install - can add "{module_path}" to the users PATH (via bashrc) if needed
        subprocess.call(['ansible-playbook'] + user_option + ['-v', '-e', '"root_prefix={0}"'.format(install_directory),
            '-e', '"@../environment_module.json"',
            '--ssh-extra-args=-o PasswordAuthentication=true -o PreferredAuthentications=password -o KbdInteractiveAuthentication=yes -o StrictHostKeyChecking=no', 
            '-i', '../inventory.txt', 'install-requirements.yml'] + connection_option, cwd='./ansible')

    except BaseException as e:
        print ('Install threw error {0} at line {1}'.format(str(e), sys.exc_info()[-1].tb_lineno))

if __name__ == '__main__':
    install()

