# EasyBlocks

This is the place to put all software specific easyblocks. It should be flat list, so no subdirs.

All generic easyblocks should go into the `easyblocks/generic` directory.

# Installation

Please make sure that you add `EASYBUILD_INCLUDE_EASYBLOCKS=/PATH/TO/THIS/REPO/easyblocks/\*.py, /PATH/TO/THIS/REPO/easyblocks/generic/\*.py` somewhere to the environment.
