# EasyBlocks

This is the place to put all generic easyblocks. It should be flat list, so no subdirs.

All software specific easyblocks should go into the root of the `easyblocks` directory.

# Installation

Please make sure that you add `EASYBUILD_INCLUDE_EASYBLOCKS=/PATH/TO/THIS/REPO/easyblocks/\*.py, /PATH/TO/THIS/REPO/easyblocks/generic/\*.py` somewhere to the environment.
