Easybuild easy config files for fastr pipeline tools
====================================================


Usage
-----

These *.eb files are intended to used together with a standard EasyBuild
installation. Once installed EasyBuild comes with a standard set of *.eb
files. The files in this repository can be cloned to a fast specific
directory that then must be added to the 
[EasyBuild configuration.](http://easybuild.readthedocs.io/en/latest/Using_the_EasyBuild_command_line.html#controlling-robot-search-path) 

Install the BBMRI/fastr environment and pipelines from scratch on a target machine
============================================================================
Its not necessary to clone this or other repositories. The following guide contains an install 
script (currently only Linux is supported) that will downloas all the BBMRI and fastr resources needed
and take you step by step through the install of fastr and allow you to choose from pipelines.
In the text "host machine" refers to the system running the install process "target machine" refers to the
system on which BBMRI/fastr environment will be installed. These can be one and the same system. 

1. Prerequisites:
    * Host 
        * Reqular user account with nnnMB disk space free
        * Python version 2.6 or above 
        * Ansible 2.0 or above
    * Target
        * SSH access (if not the same machine as host)
        * Regular user account with nnnMB disk space free
        * Build tools
            * CentOS/RedHat: 'Development Tools'
            * Debian/Ubuntu: build-essential
2. Make and empty directory and get the build script, and make it executable
    * curl -O https://gitlab.com/bbmri/pipelines/deploy-resources/raw/master/installer.sh 
        (if you prefer can be done using curl)
    * chmod +x installer.sh
3. Run the installer.sh and follow the instructions
    * Note: You will be prompted for your gitlab username - give the username you use to access this site if your have one. If you have access to [deploy-resources-bbmri](https://gitlab.com/bbmri/pipelines/deploy-resources) then the bbmri private pipelines will also be installed 